<?php

namespace Drupal\listformat_example\BuildProvider;

use Drupal\cfrapi\Configurator\Configurator_IntegerInRange;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\renderkit\BuildProvider\BuildProvider;
use Drupal\renderkit\BuildProvider\BuildProviderInterface;
use Drupal\renderkit\ListFormat\ListFormat;
use Drupal\renderkit\ListFormat\ListFormatInterface;

/**
 * A special BuildProvider* class to demonstrate ListFormat* functionality.
 *
 * Long-term, this belongs into renderkit, with an annotation to expose it as a
 * plugin. However, to make this work with older versions of renderkit, we put a
 * copy/preview of this class here.
 *
 * @see _listformat_example_page()
 */
class BuildProvider_Repeat implements BuildProviderInterface {

  /**
   * @var \Drupal\renderkit\BuildProvider\BuildProviderInterface
   */
  private $provider;

  /**
   * @var \Drupal\renderkit\ListFormat\ListFormatInterface
   */
  private $listFormat;

  /**
   * @var int
   */
  private $n;

  /**
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function plugin(CfrContextInterface $context = NULL) {
    return Configurator_CallbackConfigurable::createFromClassName(
      __CLASS__,
      [
        BuildProvider::configurator($context),
        ListFormat::configurator($context),
        new Configurator_IntegerInRange(1, 100),
      ],
      [
        t('Element to repeat'),
        t('List format'),
        t('Number of repetitions'),
      ]);
  }

  /**
   * @param \Drupal\renderkit\BuildProvider\BuildProviderInterface $provider
   * @param \Drupal\renderkit\ListFormat\ListFormatInterface $listFormat
   * @param int $n
   */
  public function __construct(BuildProviderInterface $provider, ListFormatInterface $listFormat = NULL, $n) {
    $this->provider = $provider;
    $this->listFormat = $listFormat;
    $this->n = $n;
  }

  /**
   * @return array
   *   A render array.
   */
  public function build() {
    $build = $this->provider->build();
    if ([] === $build) {
      return [];
    }
    $builds = array_fill(0, $this->n, $build);
    if ([] === $builds) {
      return [];
    }
    if (NULL !== $this->listFormat) {
      $builds = $this->listFormat->buildList($builds);
    }
    return $builds;
  }
}
