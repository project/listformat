<?php

namespace Drupal\listformat_example\ConfToForm;

use Drupal\cfrapi\ConfToForm\ConfToFormInterface;

/**
 * Proxy class for a Configurator* / ConfToForm* component.
 *
 * This avoids serialization problems in forms with 'cfrapi' element type.
 * Instead of adding the Configurator* object into the '#cfrapi_confToForm'
 * property, one can add this proxy object.
 *
 * Long-term this problem should be fixed within cfrapi, but we put a copy of
 * this here to support older versions of cfr:cfrapi.
 *
 * @see \cfrapi_element_info()
 */
class ConfToForm_ConfiguratorFactory implements ConfToFormInterface {

  /**
   * @var callable
   */
  private $factory;

  /**
   * @param string $class
   * @param string $method
   *
   * @return \Drupal\listformat_example\ConfToForm\ConfToForm_ConfiguratorFactory
   */
  public static function fromStaticMethod($class, $method) {
    return new self([$class, $method]);
  }

  /**
   * @param callable $factory
   */
  public function __construct($factory) {
    $this->factory = $factory;
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param string|null $label
   *   Label for the form element, specifying the purpose where it is used.
   *
   * @return array
   */
  public function confGetForm($conf, $label) {
    $confToForm = call_user_func($this->factory);
    if (!$confToForm instanceof ConfToFormInterface) {
      return [];
    }
    return $confToForm->confGetForm($conf, $label);
  }
}
